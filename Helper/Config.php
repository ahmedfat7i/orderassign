<?php

namespace Order\AssignTo\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Authorization;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\User\Model\ResourceModel\User\Collection;
use Magento\User\Model\User;

class Config
{
    const ENABLED = 'enable';
    const USER_ROLES = 'user_roles';
    const ACL = 'Order_AssignTo::configuration';
    const URL = 'order/user/assign';
    const PARAM = 'user';
    const LABEL = 'assign_to';
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var UrlInterface
     */
    private $urlBuilder;
    /**
     * @var Collection
     */
    private $userCollection;
    /**
     * @var Authorization
     */
    private $authorization;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param UrlInterface         $urlBuilder
     * @param Collection           $userCollection
     * @param Authorization        $authorization
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        UrlInterface         $urlBuilder,
        Collection           $userCollection,
        Authorization        $authorization
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->urlBuilder = $urlBuilder;
        $this->userCollection = $userCollection;
        $this->authorization = $authorization;
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    protected function getConfig($key)
    {
        return $this->scopeConfig->getValue(
            'order_assign_to/general/' . $key,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->getConfig(self::ENABLED) && $this->authorization->isAllowed($this::ACL);
    }

    /**
     * @return false|string[]
     */
    public function getUserRoles()
    {
        return explode(',', $this->getConfig(self::USER_ROLES));
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        $options = [];
        if ($this->isEnabled()) {
            $users = $this->userCollection
                ->addFieldToFilter("detail_role.role_name", ['in' => $this->getUserRoles()]);
            /** @var User $user */
            foreach ($users as $user) {
                $options [] = [
                    'value'         => $user->getUserName(),
                    'label'         => $user->getName(),
                    'type'          => self::LABEL . $user->getName(),
                    '__disableTmpl' => true,
                    'url'           => $this->urlBuilder->getUrl(
                        $this::URL,
                        [$this::PARAM => $user->getUserName()]
                    )
                ];
            }
        }
        return $options;
    }
}
