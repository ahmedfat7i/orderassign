<?php

namespace Order\AssignTo\Controller\Adminhtml\User;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Order\AssignTo\Helper\Config;

class Assign extends AbstractMassAction
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Context                $context
     * @param Filter                 $filter
     * @param Config                 $config
     * @param CollectionFactory      $collectionFactory
     */
    public function __construct(
        Context                $context,
        Filter                 $filter,
        Config                 $config,
        CollectionFactory      $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->config = $config;
        parent::__construct($context, $filter);
    }

    /**
     * Cancel selected orders
     *
     * @param AbstractCollection $collection
     *
     * @return Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        try {
            if ($this->config->isEnabled()) {
                if ($collection->getSize()) {
                    $collection->setDataToAll('assigned_to', $user = $this->getRequest()->getParam('user'));
                    $collection->save();
                    $this->messageManager->addSuccessMessage(__('order(s) assigned to  %1 .', $user));
                } else {
                    $this->messageManager->addErrorMessage(__('You cannot assign the selected order(s).'));
                }
            } else {
                $this->messageManager->addErrorMessage(__('You are not allowed to assign order(s).'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('You cannot assign the selected order(s).'));
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order/index');
        return $resultRedirect;
    }
}
