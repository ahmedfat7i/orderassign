<?php

namespace Order\AssignTo\Ui\Component\MassAction;

use Magento\Framework\AuthorizationInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\MassAction;
use Order\AssignTo\Helper\Config;

class Restrictions extends MassAction
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param ContextInterface $context
     * @param Config           $config
     * @param array            $components
     * @param array            $data
     */
    public function __construct(
        ContextInterface $context,
        Config           $config,
        array            $components,
        array            $data
    )
    {
        $this->config = $config;
        parent::__construct($context, $components, $data);
    }

    public function prepare()
    {
        parent::prepare();
        $config = $this->getConfiguration();
        $allowedActions = [];
        foreach ($config['actions'] as $action) {
            if ($this->config::LABEL == $action['type'] && !$this->config->isEnabled()) {
                continue;
            } else {
                $allowedActions[] = $action;
                $types [] = $action['type'];
            }
        }
        $config['actions'] = $allowedActions;
        $this->setData('config', $config);
    }
}
