<?php

namespace Order\AssignTo\Ui\Component\MassAction;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\MassAction;
use Order\AssignTo\Helper\Config;

class AssignAction extends MassAction
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param ContextInterface $context
     * @param Config           $config
     * @param array            $components
     * @param array            $data
     */
    public function __construct(
        ContextInterface $context,
        Config           $config,
        array            $components = [],
        array            $data = []
    )
    {
        $this->config = $config;
        parent::__construct($context, $components, $data);
    }

    public function prepare()
    {
        $config = $this->getConfiguration();
        $config['actions'] = $this->config->getUsers();
        $this->setData('config', $config);
        $this->components = [];
        parent::prepare();
    }
}
